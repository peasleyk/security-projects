# Search-able Encryption Report

Kyle Peasley


## Set up

OS: Linux

Python version:  3.6.6

Windows should work fine, as well as well as a different Python3 version. ***Python3*** is needed as I uses the ```secrets``` module, which generates bits more securely than the ```random``` module.

If you use python 2 you will receive a message telling you to use 3

##### Third party libraries

###### pip

I used the library "PyCrypto". You can install this using pip. For example,

```shell
pip3 install --user PyCrypto
```

This is the easiest way. You can then use the program as normal.

###### pipenv

I personally use pipenv and have provided a pipfile. If using this, you can run

```shell
pipenv install --three
```

 You can then run

```shell
pipenv run python3 main.py
```

along with any parameters listed in usage.

## Usage

Below is the usage output. Please run from the ```src``` folder for the default file locations to work, e.g. `./se_05745685/src`

```shell
usage: main.py [-h] [-k KEYGEN] [-kfaes KEYFILEAES] [-kfprf KEYFILEPRF]
               [-iv IVFILE] [-i] [-if INDEXFILE] [-f FILES]
               [-co CIPHERTEXTOUT] [-t TOKEN] [-to TOKENFILE] [-s]
               [-rf RESULTFILE]

optional arguments:
  -h, --help            show this help message and exit
  -k KEYGEN, --keygen KEYGEN
                        Generate iv.txt with <x> bits
  -kfaes KEYFILEAES, --keyfileAES KEYFILEAES
                        Location of the keyfile to encrypt with or decrypt
                        with
  -kfprf KEYFILEPRF, --keyfilePRF KEYFILEPRF
                        Location of the keyfile to encrypt with or decrypt
                        with
  -iv IVFILE, --ivFile IVFILE
                        Location to save iv
  -i, --index           Create an inverted index
  -if INDEXFILE, --indexFile INDEXFILE
                        Location to create the inverted index
  -f FILES, --files FILES
                        Location of the data files (f1.txt, etc)
  -co CIPHERTEXTOUT, --ciphertextOut CIPHERTEXTOUT
                        Location to save encrypted keyword files
  -t TOKEN, --token TOKEN
                        Generate a token from a word using a prfstring
  -to TOKENFILE, --tokenFile TOKENFILE
                        Location to save token
  -s, --search          Search for a token in the inverted index
  -rf RESULTFILE, --resultFile RESULTFILE
                        Location to save search result
```

The four main flags to run the program are

- ```python3 main.py -k <bits> ``` (Generate a key)
- ```python3 main.py -i``` (Create index file)
- ```python3 main.py -t <word>``` (Create a token to search)
- ```python3 main.py -s``` (Search the index with that token)

#### Keygen

Here is the output for key generation

![](/home/pita/Classes/y4s1/DataPrivacy/Assignments/3/se_05745685/Screenshot_2018-11-20_19-17-31.png)

Choosing a location

![](/home/pita/Classes/y4s1/DataPrivacy/Assignments/3/se_05745685/Screenshot_2018-11-20_19-18-37.png)

#### Encryption.Index

Here is the output for creating an index/encrypting.

![1542759804329](/tmp/pita/1542759804329.png)

Here is an output specifying locations

![1542759926593](/tmp/pita/1542759926593.png)

#### Token generation

Here is the output for creating a token

![1542766344957](/tmp/pita/1542766344957.png)

Similarly you can choose different values for the defaults

![1542766391601](/tmp/pita/1542766391601.png)

#### Search function

Here's the output when searching for a valid keyword. Keywords that don't exist return nothing.

![1542760074041](/tmp/pita/1542760074041.png)

Again, flags to change defaults

![1542760200236](/tmp/pita/1542760200236.png)

## Architecture and Speed

#### Architecture

The search-able encryption is implemented using AES256 for encrypting, and SHA256 for the keywords. The key for generating a keyword will always be empty as it just hashes.

 The index is stored as a hashtable, where the value is a set to remove duplicates. This is then converted to a list before being wrote to disk. The index is written as json to make it easy to load and save the index to disk.

#### Speed tests

##### Generating an index

![1542766687249](/tmp/pita/1542766687249.png)

This could probably be faster without printing

##### Search and decryption

The times of creating the token and searching and decrypting

![1542766649843](/tmp/pita/1542766649843.png)
