import os
import sys
import argparse
import hashlib
import json
from collections import defaultdict

try:
  from Crypto.Cipher import AES
  from Crypto.Hash import SHA256
except ImportError:
  print("PyCrypto library not installed. Please see report on how to install. Exiting...")
  sys.exit()

try:
  # Key/iv generation
  import secrets
except ImportError:
  print("Please use python3. See report. Exiting...")

class cipher():
  """
  Small class to hold methods dealing with encrypting and unecrypting
  """
  def __init__(self, keyLoc, IVLoc):
    self.key = readFile(keyLoc, binary=True)
    self.iv = readFile(IVLoc, binary=True)

  def AESencrypt(self, plaintext, newName, output):
    """
    Encrypts the given plaintext file with the given key file,
    and writes the output to ciphertext.txt, overwriting ciphertext.txt it if it exists
    Uses AES
    """
    message = readFile(plaintext)
    message = self.stringToBytes(message)
    try:
      cipher = AES.new(self.key, AES.MODE_CFB, self.iv)
    except Exception as e:
      print("Error creating cipher: {}".format(e))
      sys.exit()
    ciphertext = cipher.encrypt(message)
    writeFile(newName, ciphertext, binary=True)

  def AESdecrypt(self, ciphertextLoc):
    """
    Decrypts the given ciphertext with then given key and writes it
    to result.txt, overwriting result.txt if it exists.
    """
    ciphertext = readFile(ciphertextLoc, binary=True)
    try:
      cipher = AES.new(self.key, AES.MODE_CFB, self.iv)
    except Exception as e:
      print("Error creating cipher: {}".format(e))
      sys.exit()
    plaintext = self.binaryToString(cipher.decrypt(ciphertext).decode())
    return plaintext

  def stringToBytes(self, string):
    """
    Converts a string of characters, e.g. "tree" to a string of bytes,
    "10110101"
    """
    return ''.join(format(x,'b').zfill(8) for x in bytearray(str.encode(string)))

  def binaryToString(self, binary):
    """
    Converts a binary string, e.g. "110110" to a character string, "tree"
    """
    chars = []
    for x in range(0, len(binary), 8):
      char = binary[x:x+8]
      chars.append(char)
    m = ''.join(chr(int(x,2)) for x in chars)
    return m

def convertFilename(string):
  """
  Converts "file" to "encrypted_file"
  """
  rename = "encrypted_{}".format(string)
  return rename

def getFiles(location):
  """
  Returns only files in a directory, not folders
  """
  files = []
  for file in os.listdir(location):
    if os.path.isfile(os.path.join(location,file)):
      files.append(file)
  return files

def readFile(location, binary=False):
  """
  Reads in a file from a location and returns the output
  """
  if not os.path.exists(location):
    print("File {} not found, exiting".format(location))
    sys.exit()
  if binary:
    with open(location, "rb") as f:
      data = f.read()
  else:
    with open(location, "r") as f:
      data = f.read()
  f.close()
  return(data)

def writeFile(location, message, binary=False):
  """
  Takes in a file location and a string and writes it
  to the given file, overwriting it.
  """
  if os.path.exists(location):
    os.remove(location)
  if not os.path.exists(os.path.dirname(location)):
    os.makedirs(os.path.dirname(location))
  if binary:
    with open(location, "wb") as f:
      f.write(message)
  else:
    with open(location, "w") as f:
      f.write(message)

  f.close()
  print("Wrote to {}".format(location))

def encryptKeywords(filesLoc, outLoc, Cipher):
  """
  Runs though the files folder, and encrypts eachone with AES
  """
  print("Encrypting plaintext files...")
  files = getFiles(filesLoc)
  for file in files:
    fullPath = os.path.join(filesLoc, file)
    rename = os.path.join(outLoc, convertFilename(file))
    Cipher.AESencrypt(fullPath, rename, outLoc)

def PRFstring(plaintext):
  """
  Hashes the given string  with SHA256
  """
  sha = SHA256.new()
  sha.update(str.encode(plaintext))
  hashed = bytes.hex(sha.digest())
  return hashed

def createIndex(filesLoc, outLoc):
  """
  Runes though the files folder, finds keywords, and hashes them
  Uses those hashes to create an encrypted inverted index
  """
  print("Generating hashes")
  index = defaultdict(set)
  files = getFiles(filesLoc)
  for file in files:
    rename = convertFilename(file)
    keywords = readFile(os.path.join(filesLoc, file)).strip()
    for keword in keywords.split(" "):
      hashed = PRFstring(keword)
      index[hashed].add(rename)

  # Lazily Converts the sets so we can serialize it
  for key in index.keys():
    index[key] = list(index[key])

  with open(outLoc, "w") as f:
    json.dump(index, f)

def keygen(keyLoc, bits):
  """
  Generates a key using AES, in base16
  AES key must be either 16, 24, or 32 bytes long
  """
  print("Generating key with {} bits".format(bits))
  key = ""
  if bits > 0:
    key = secrets.token_bytes(bits // 8)
    writeFile(keyLoc, key, binary=True)
  else:
    writeFile(keyLoc, key, binary=False)

  print("Key: {}".format(key))

def ivGen(ivLoc):
  """
    Generates a 16 bit IV, the size of the CBC block
  """
  print("Generating IV")
  key = secrets.token_bytes(16)
  print("IV: {}".format(key))
  writeFile(ivLoc, key, binary=True)

def search(token, index, cipherFiles, outLoc, Cipher):
  """
  Searches for the tokens hash in the index
  If files are found, decrypts them and prints their plaintext
  """
  with open(index, "r") as f:
    cIndex = json.load(f)

  if token not in cIndex.keys():
    print("No files found for given token")
    return
  matching = cIndex[token]
  print("Locations of token in files: {}".format(matching))
  print("Decrypted contents of those files:")
  output = []
  for file in matching:
    fullPath = os.path.join(cipherFiles, file) #"{}/{}".format(cipherFiles, file)
    plaintext = Cipher.AESdecrypt(fullPath)
    line = "{}: {}".format(file, plaintext.strip())
    output.append(line)
    print(line)
  writeFile(outLoc, "\n".join(output))

def main():
  parser = argparse.ArgumentParser()

  parser.add_argument("-k", "--keygen",
          help="Generate iv.txt with <x> bits", type=int)
  parser.add_argument("-kfaes", "--keyfileAES",
          help="Location of the keyfile to encrypt with or decrypt with",
          default="../data/skaes.txt")
  parser.add_argument("-kfprf", "--keyfilePRF",
          help="Location of the keyfile to encrypt with or decrypt with",
          default="../data/skprf.txt")
  parser.add_argument("-iv", "--ivFile",
          help="Location to save iv",
          default="../data/iv.txt")

  parser.add_argument("-i", "--index",
          help="Create an inverted index", action='store_true')
  parser.add_argument("-if", "--indexFile",
          help="Location to create the inverted index",
          default="../data/index.json")
  parser.add_argument("-f", "--files",
          help="Location of the data files (f1.txt, etc)",
          default="../data/files")
  parser.add_argument("-co", "--ciphertextOut",
          help="Location to save encrypted keyword files",
          default="../data/ciphertextfiles")

  parser.add_argument("-t", "--token",
          help="Generate a token from a word using a prfstring", type=str)
  parser.add_argument("-to", "--tokenFile",
          help="Location to save token",
          default="../data/token.txt")

  parser.add_argument("-s", "--search",
          help="Search for a token in the inverted index", action='store_true')
  parser.add_argument("-rf", "--resultFile",
          help="Location to save search result",
          default="../data/result.txt")

  parser.parse_args()
  flags = vars(parser.parse_args())

  if len(sys.argv) == 1:
    parser.print_help()

  if flags["keygen"]:
    bits = flags["keygen"]
    keygen(flags["keyfileAES"], bits)
    keygen(flags["keyfilePRF"], 0)
    ivGen(flags["ivFile"])

  if flags["index"]:
    location = flags['files']
    output = flags['indexFile']
    createIndex(location, output)

    key = flags['keyfileAES']
    output = flags['ciphertextOut']
    IV = flags['ivFile']
    Cipher = cipher(key, IV)
    encryptKeywords(location, output, Cipher)

  if flags["token"]:
    word = flags["token"]
    output = flags["tokenFile"]
    token = PRFstring(word)
    print("Token for {}: {}".format(word, token))
    writeFile(output, token)

  if flags["search"]:
    token = readFile(flags["tokenFile"])
    cipherFiles = flags['ciphertextOut']
    index = flags['indexFile']
    key = flags["keyfileAES"]
    IV = flags['ivFile']
    Cipher = cipher(key, IV)
    output = flags["resultFile"]
    search(token, index, cipherFiles, output, Cipher)

if __name__ == "__main__":
  main()
