# Symmetric-Key Encryption Report

Kyle Peasley

### Set up

OS: Linux

Python version:  3.6.6

Windows should work fine, as well as well as a different Python3 version. ***Python3*** is needed as I uses the ```secrets``` module, which generates bits more securely than the ```random``` module. 

If you use python 2 you will receive a message telling you to use 3

![1539203463973](/tmp/pita/1539203463973.png)

##### Third party libraries

I used the library "PyCrypto". You can install this using pip. For example,

```shell
pip3 install --user PyCrypto
```

This is the easiest way

I personally use pipenv and have provided a pipfile. If using this, you can run

```shell
pipenv install --three
```

A virtualenv will be created. You can then run 

```shell
pipenv shell
```

and use the library from the virtualenv.

### Usage

Below is the usage output. Please run from the ```src``` folder for the default file locations to work, e.g. `./aes_05745685/src`

```shell
usage: main.py [-h] [-e] [-d] [-k] [-kf KEYFILE] [-iv IVFILE] [-pf PLAINTEXT]
               [-cf CIPHERTEXT] [-dr DECRESULT]

optional arguments:
  -h, --help            show this help message and exit
  -e, --encrypt         Encrypt plaintext.txt with key.txt
  -d, --decrypt         Decrypt ciphertext.txt with key.txt
  -k, --keygen          Generate key.txt with 256 bits
  -kf KEYFILE, --keyfile KEYFILE
                        Location of the keyfile to encrypt with or decrypt
                        with
  -iv IVFILE, --ivFile IVFILE
                        Location of the ivFile to encrypt with or decrypt with
  -pf PLAINTEXT, --plaintext PLAINTEXT
                        Location of the plaintext file to encrypt from
  -cf CIPHERTEXT, --ciphertext CIPHERTEXT
                        Location of the ciphertext file to encrypt to or
                        decrypt from
  -dr DECRESULT, --decresult DECRESULT
                        Location of the decryption result when decrypting


```

The three flags needed to encrypt, decrypt, and generate a keyfile are --encrypt, --decrypt, and --keygen.

#### Keygen

Here is the output for key generation

![1539203116248](/tmp/pita/1539203116248.png)

Choosing a location

![1539203187272](/tmp/pita/1539203187272.png)

#### Encryption

Here is the output for encryption.

![1539203207006](/tmp/pita/1539203207006.png)

The location of the plaintext input, ciphertext output, and key input can also be chosen. The default output picks them up from the ```data``` folder.

![1539203378817](/tmp/pita/1539203378817.png)

#### Decryption

Here is the output for decryption

![1539203401393](/tmp/pita/1539203401393.png)

Similarly you can choose different values for the defaults

![1539203429844](/tmp/pita/1539203429844.png)

