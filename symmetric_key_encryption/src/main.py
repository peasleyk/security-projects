import os
import sys
import argparse

try:
  from Crypto.Cipher import AES
except ImportError:
  print("PyCrypto library not installed. Please see report on how to install. Exiting...")
  sys.exit()

try:
  import secrets
except ImportError:
  print("Please usepython3. See report. Exiting...")

def readFile(location):
  """
  Reads in a file from a location and returns the output
  """
  if not os.path.exists(location):
    print("File {} not found, exiting".format(location))
    sys.exit()
  with open(location, "r") as f:
    data = f.read()
  f.close()
  return(data)

def writeFile(location, message):
  """
  Takes in a file location and a string and writes it
  to the given file, overwriting it.
  """
  if os.path.exists(location):
    os.remove(location)
  with open(location, "w") as f:
    f.write(message)
  f.close()
  print("Wrote to {}".format(location))

def stringToBytes(string):
  """
  Converts a string of characters, "tree" to a string of bytes,
  "10110101"
  """
  return ''.join(format(x,'b').zfill(8) for x in bytearray(str.encode(string)))

def binaryToString(binary):
  """
  Converts a binary string, "110110" to a character string, "tree"
  """
  chars = []
  for x in range(0, len(binary), 8):
    char = binary[x:x+8]
    chars.append(char)
  m = ''.join(chr(int(x,2)) for x in chars)
  return m

def encrypt(plaintext, keyLoc, IVLoc, output):
  """
  Encrypts the given plaintext file with the given key file,
  and writes the output to ciphertext.txt, overwriting ciphertext.txt it if it exists
  """
  print("Encrypting plaintext...")
  message = readFile(plaintext)
  message = stringToBytes(message)

  # We must convert from hex string to bytes
  key = bytes.fromhex(readFile(keyLoc))
  IV = bytes.fromhex(readFile(IVLoc))
  cipher = AES.new(key, AES.MODE_CFB, IV)
  ciphertext = bytes.hex(cipher.encrypt(message))
  writeFile(output, ciphertext)

def decrypt(ciphertextLoc, keyLoc, IVLoc, output):
  """
  Decrypts the given ciphertext with then given key and writes it
  to result.txt, overwriting result.txt if it exists.
  """
  print("Decrypting ciphertext..")
  ciphertext = bytes.fromhex(readFile(ciphertextLoc))
  key = bytes.fromhex(readFile(keyLoc))
  IV = bytes.fromhex(readFile(IVLoc))
  cipher = AES.new(key, AES.MODE_CFB, IV)
  plaintext = binaryToString(cipher.decrypt(ciphertext).decode())
  print("Decrypted ciphertext: {}".format(plaintext))
  writeFile(output, plaintext)


def keygen(keyLoc):
  """
  Generates a key using AES, in base16
  """
  print("Generating key")
  # 256 bytes in 32 bits
  key = secrets.token_hex(32)
  print("Key: {}".format(key))
  writeFile(keyLoc,key)

def ivGen(ivLoc):
  """
    Generates a 16 bit IV, the size of the CBC block
  """
  print("Generating IV")
  key = secrets.token_hex(16)
  print("IV: {}".format(key))
  writeFile(ivLoc,key)

def main():
  parser = argparse.ArgumentParser()

  parser.add_argument("-e", "--encrypt",
          help="Encrypt plaintext.txt with key.txt", action='store_true')
  parser.add_argument("-d", "--decrypt",
          help="Decrypt ciphertext.txt with key.txt", action='store_true')
  parser.add_argument("-k", "--keygen",
          help="Generate iv.txt with 256 bits", action="store_true")
  parser.add_argument("-kf", "--keyfile",
          help="Location of the keyfile to encrypt with or decrypt with",
          default="../data/key.txt")
  parser.add_argument("-iv", "--ivFile",
          help="Location of the ivFile to encrypt with or decrypt with",
          default="../data/iv.txt")
  parser.add_argument("-pf", "--plaintext",
          help="Location of the plaintext file to encrypt from",
          default="../data/plaintext.txt")
  parser.add_argument("-cf", "--ciphertext",
          help="Location of the ciphertext file to encrypt to or decrypt from",
          default="../data/ciphertext.txt")
  parser.add_argument("-dr", "--decresult",
          help="Location of the decryption result when decrypting",
          default="../data/result.txt")

  parser.parse_args()
  flags = vars(parser.parse_args())

  if flags["encrypt"]:
    ivGen(flags["ivFile"])
    encrypt(flags["plaintext"], flags["keyfile"], flags["ivFile"], flags["ciphertext"])
  if flags["decrypt"]:
    decrypt(flags["ciphertext"],flags["keyfile"], flags["ivFile"], flags["decresult"])
  if flags["keygen"]:
    keygen(flags["keyfile"])
  if len(sys.argv) == 1:
    parser.print_help()

if __name__ == "__main__":
  main()
