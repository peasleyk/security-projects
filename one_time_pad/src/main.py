import os
import sys
import secrets
import argparse

def readFile(location):
  """
  Reads in a file from a location and returns the output
  """
  if not os.path.exists(location):
    print("File {} not found, exiting".format(location))
    sys.exit()
  with open(location, "r") as f:
    data = f.read()
  f.close()
  return(data)

def writeFile(location, message):
  """
  Takes in a file location and a string and writes it
  to the given file, overwriting it.
  """
  if os.path.exists(location):
    os.remove(location)
  with open(location, "w") as f:
    f.write(message)
  f.close()

def stringToBytes(string):
  """
  Converts a string of characters, "tree" to a string of bytes,
  "10110101"
  """
  return ''.join(format(x,'b').zfill(8) for x in bytearray(str.encode(string)))

def xor(m, sk):
  """
  Xors two strings of bytes together and returns the output
  """
  xored = ""
  for x in range(0, len(m)):
    xored += str(int(m[x]) ^ int(sk[x]))
  return xored

def binaryToString(binary):
  """
  Converts a binary string, "110110" to a character string, "tree"
  """
  chars = []
  for x in range(0, len(binary), 8):
    char = binary[x:x+8]
    chars.append(char)
  m = ''.join(chr(int(x,2)) for x in chars)
  return m

def encrypt(key, plaintext, output):
  """
  Encrypts the given plaintext file with the given key file,
  and writes the output to ciphertext.txt, overwriting ciphertext.txt it if it exists
  """
  print("Encrypting plaintext")
  m = readFile(plaintext)
  if isinstance(m, str):
    m = stringToBytes(m)

  sk = readFile(key)

  if len(m) == len(sk):
    otp = xor(m,sk)
    print("m  : {} \nsk : {} \notp: {}".format(m, sk, otp))
    writeFile(output, otp)
  else:
    print("Message length and key length not the same")
    writeFile(output, "")

def decrpt(ciphertext, key, output):
  """
  Decrypts the given ciphertext with then given key and writes it
  to result.txt, overwriting result.txt if it exists.
  """
  print("Decrypting ciphertext")
  c = readFile(ciphertext)
  sk = readFile(key)

  if len(c) == len(sk):
    m = xor(c ,sk)
    m = binaryToString(m)
    print("m (string): {} \nsk        : {} \nc         : {}".format(m, sk, c))
    writeFile(output, m)
  else:
    print("Ciphertext length and key length not the same")
    writeFile(output, "")

def keygen(klen, keyLoc):
  print("Generating key")
  if klen < 1 or klen > 127:
    print("Key length of {} out of range".format(klen))
    return
  key = ""
  for x in range(0, klen):
    key += str(secrets.randbits(1))
  print(key)
  writeFile(keyLoc, str(key))

def main():
  parser = argparse.ArgumentParser()

  parser.add_argument("-e", "--encrypt",
          help="Encrypt plaintext.txt with key.txt", action='store_true')
  parser.add_argument("-d", "--decrypt",
          help="Decrypt ciphertext.txt with key.txt", action='store_true')
  parser.add_argument("-k", "--keygen",
          help="Generate newkey.txt with KEYGEN bits",
          type=int)
  parser.add_argument("-kf", "--keyfile",
          help="Location of the keyfile to encrypt with or decrypt with",
          default="../data/key.txt")
  parser.add_argument("-mf", "--plaintext",
          help="Location of the plaintext file to encrypt from",
          default="../data/plaintext.txt")
  parser.add_argument("-cf", "--ciphertext",
          help="Location of the ciphertext file to encrypt to or decrypt from",
          default="../data/ciphertext.txt")
  parser.add_argument("-nk", "--newkey",
          help="Location to create the new key when generating",
          default="../data/newkey.txt")
  parser.add_argument("-dr", "--decresult",
          help="Location of the decryption result when decrypting",
          default="../data/result.txt")

  parser.parse_args()
  flags = vars(parser.parse_args())

  if flags["encrypt"]:
    encrypt(flags["keyfile"], flags["plaintext"], flags["ciphertext"])
  if flags["decrypt"]:
    decrpt(flags["ciphertext"],flags["keyfile"], flags["decresult"])
  if flags["keygen"]:
    keygen(flags["keygen"], flags["newkey"])
if __name__ == "__main__":
  main()
