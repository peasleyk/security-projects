# One-Time Pad report

Kyle Peasley

### Set up

OS: Linux

Python version:  3.6.6

Windows should work fine, as well as well as a different Python3 version. ***Python3*** is needed as I uses the ```secrets``` module, which generates bits more securely than the ```random``` module. 

If you use python 2 you will receive the following import error:

```python
Traceback (most recent call last):
  File "main.py", line 3, in <module>
    import secrets
ImportError: No module named secrets

```

### Usage

Below is the usage output. Please run from the ```src``` folder for the default file locations to work,

```shell
 python main.py -h
usage: main.py [-h] [-e] [-d] [-k KEYGEN] [-kf KEYFILE] [-mf PLAINTEXT]
               [-cf CIPHERTEXT] [-nk NEWKEY] [-dr DECRESULT]

optional arguments:
  -h, --help            show this help message and exit
  -e, --encrypt         Encrypt plaintext.txt with key.txt
  -d, --decrypt         Decrypt ciphertext.txt with key.txt
  -k KEYGEN, --keygen KEYGEN
                        Generate newkey.txt with KEYGEN bits
  -kf KEYFILE, --keyfile KEYFILE
                        Location of the keyfile to encrypt with or decrypt
                        with
  -mf PLAINTEXT, --plaintext PLAINTEXT
                        Location of the plaintext file to encrypt from
  -cf CIPHERTEXT, --ciphertext CIPHERTEXT
                        Location of the ciphertext file to encrypt to or
                        decrypt from
  -nk NEWKEY, --newkey NEWKEY
                        Location to create the new key when generating
  -dr DECRESULT, --decresult DECRESULT
                        Location of the decryption result when decrypting

```

The three flags needed to encrypt, decrypt, and generate a keyfile are --encrypt, --decrypt, and --keygen BITS . Keygen requires a int BITS after the flag



#### Encryption

Here is the output for encryption.

![1537735031559](/tmp/pita/1537735031559.png)

The location of the plaintext input, ciphertext output, and key input can also be chosen. The default output picks them up from the ```data``` folder.

![1537735053512](/tmp/pita/1537735053512.png)

#### Decryption

Here is the output for decryption

![1537735533590](/tmp/pita/1537735533590.png)

Similarly you can choose different values for the defaults

![1537735599880](/tmp/pita/1537735599880.png)

#### Keygen

Here is the output for key generation with 100 bits

![1537735273508](/tmp/pita/1537735273508.png)

Choosing a location

![1537735301737](/tmp/pita/1537735301737.png)