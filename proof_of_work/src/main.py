import os
import sys
import argparse

try:
  from Crypto.Hash import SHA256
except ImportError:
  print("PyCrypto library not installed. Please see report on how to install. Exiting...")
  sys.exit()

class Cipher():
  """
  Small class to hold methods dealing with
  """
  def __init__(self):
    return

  def hash(self, text, nonce):
    """
    Hashes the given value with SHA256 and returns binary representation
    """
    sha = SHA256.new()
    sha.update(str.encode(text))
    sha.update(str.encode(str(nonce)))
    # A little annoying in this library
    # Get the string hex value
    hashed_hex = bytes.hex(sha.digest())
    # Convert that to an int
    hashed_number = int(hashed_hex,16)
    # Convert that to binary
    hashed_binary = f'{hashed_number:0>42b}'
    # Even though it shouldn't remove leading zeros it does
    # so add them back
    return hashed_binary.zfill(256)


def readFile(location, binary=False):
  """
  Reads in a file from a location and returns the output
  """
  if not os.path.exists(location):
    print("File {} not found, exiting".format(location))
    sys.exit()
  with open(location, "r") as f:
    data = f.read()
  f.close()
  return(data.strip())

def writeFile(location, message, binary=False):
  """
  Takes in a file location and a string and writes it
  to the given file, overwriting it.
  """
  if os.path.exists(location):
    os.remove(location)
  if not os.path.exists(os.path.dirname(location)):
    os.makedirs(os.path.dirname(location))
  else:
    with open(location, "w") as f:
      f.write(message)

  f.close()
  print("Wrote to {}".format(location))

def solutionGen(Cipher, solutionLoc, targetLoc,inputLoc):
  """
  Attempts to find a nonce for the given input
  Starts at 0, adding 1 to the nonce every time
  hashes (input + nonce) then counts the zeros
  if len(zeros) is greater or equal to the difficulty it's a solution
  """
  target = readFile(targetLoc)
  inputText = readFile(inputLoc)
  print("Finding solution to target {} ".format(target))
  zeroCount = target.count("0")
  found = False
  maybeNonce = 0
  while not found:
    maybeSolution = Cipher.hash(inputText, maybeNonce)
    zeros = maybeSolution.index("1")
    if zeros >= zeroCount:
      print("Hash solution: {}" .format(maybeSolution))
      print("Nonce solution: {}".format(maybeNonce))
      writeFile(solutionLoc, str(maybeNonce))
      return
    maybeNonce+=1

def targetGen(targetLoc, difficulty):
  """
    Generates a target with a given difficulty
  """
  target = ""
  print("Generating target")
  for x in range(0,256):
    if x < difficulty:
      target += "0"
    else:
      target += "1"
  print("Target: {}".format(target))
  writeFile(targetLoc, target)

def verify(Cipher, solutionLoc, targetLoc, inputLoc):
  """
  Given a nonce, verifys that it solves the given difficulty
  for the input text
  """
  print("Verifying solution (nonce)")
  inputText = readFile(inputLoc)
  solution = int(readFile(solutionLoc))
  target = readFile(targetLoc)

  result = Cipher.hash(inputText, solution)
  zeroCount = target.count("0")
  # since we're counting leading zeros, index will give us 0001 = 3
  resultZeros = result.index("1")
  if resultZeros >= zeroCount:
    print("1")
  else:
    print("0")

def main():
  parser = argparse.ArgumentParser()

  parser.add_argument("-t", "--target",
          help="Generate target.txt with <x> difficulty", type=int)
  parser.add_argument("-s", "--solution",
          help="Generate a solution based on the target", action='store_true')
  parser.add_argument("-v", "--verify",
          help="Verifies a solution", action='store_true')
  parser.add_argument("-tf", "--targetfile",
          help="Target file",
          default="../data/target.txt")
  parser.add_argument("-i", "--inputfile",
          help="Input file",
          default="../data/input.txt")
  parser.add_argument("-sf", "--solutionfile",
          help="Solution file",
          default="../data/solution.txt")

  parser.parse_args()
  flags = vars(parser.parse_args())

  if len(sys.argv) == 1:
    parser.print_help()
    return

  if flags["target"]:
    targetGen(flags["targetfile"], flags["target"])

  if flags["solution"]:
    cipher = Cipher()
    solutionGen(cipher, flags["solutionfile"], flags["targetfile"], flags["inputfile"])

  if flags["verify"]:
    cipher = Cipher()
    verify(cipher, flags["solutionfile"], flags["targetfile"], flags["inputfile"])

if __name__ == "__main__":
  main()
