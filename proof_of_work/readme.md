# Proof of work

Kyle Peasley

### Set up

OS: Linux

Python version:  3.6.6

Windows should work fine, as well as well as a different Python version. 

Python3 is needed as I use formatting no available in 2 for getting binary. 

##### Third party libraries

###### pip

I used the library "PyCrypto". You can install this using pip. For example,

```shell
pip3 install --user PyCrypto
```

This is the easiest way

###### pipenv

I personally use pipenv and have provided a pipfile. If using this, you can run

```shell
pipenv install --three
```

A virtualenv will be created. You can then run 

```shell
pipenv shell
```

and use the library from the virtualenv.

### Usage

Below is the usage output. Please run from the ```src``` folder for the default file locations to work, e.g. `./aes_05745685/src`

```shell
pow_m0574685/src - [master●] » python main.py
usage: main.py [-h] [-t TARGET] [-s] [-v] [-tf TARGETFILE] [-i INPUTFILE]
               [-sf SOLUTIONFILE]

optional arguments:
  -h, --help            show this help message and exit
  -t TARGET, --target TARGET
                        Generate target.txt with <x> difficulty
  -s, --solution        Generate a solution based on the target
  -v, --verify          Verifies a solution
  -tf TARGETFILE, --targetfile TARGETFILE
                        Target file
  -i INPUTFILE, --inputfile INPUTFILE
                        Input file
  -sf SOLUTIONFILE, --solutionfile SOLUTIONFILE
                        Solution file
```

The Three flags needed to run the program are

```python3 main.py -t <difficulty>``` Set and create the difficulty file

```python3 main.py -s``` Search for and and save a solution

```python3 mian.py -v``` Verify the solution found when searching

#### Difficulty

Here is the output for creating a difficulty file

![1543693651364](/tmp/pita/1543693651364.png)

Choosing a location

![1543693689412](/tmp/pita/1543693689412.png)

#### Solution

Here is the output for finding a solution with difficulty 10.

![1543698173258](/tmp/pita/1543698173258.png)

Choosing locations

 ![1543698221391](/tmp/pita/1543698221391.png)

#### Verification

Here is the output for decryption

![1543698464971](/tmp/pita/1543698464971.png)

Choosing locations

![1543698448617](/tmp/pita/1543698448617.png)

